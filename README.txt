I associate prime numbers to the 26 letters and do multiplication on letters for each word to create a hash number. In this way, all words that are anagrams to each other will have the same hash number. This method makes storing and looks up easy as we only need to go through the letter of each word once to find the hash number. And look up of hash number in the word_dic is constant.  


Answer to questions:
1. 
Offline: Assuming the longest word in the dictionary has a length of N, and there are M words in the dictionary, the runtime will be O(MN) since calculating the hash_num for each word takes at most N steps and we need to repeat the calculation for M words. 

Online: Assuming there are M inputs and the max length of each input is N, the runtime will be O(MN) since calculating hash_w for each input is bounded by N and we need to repeat the calculation for M times. 

2. 
Assuming there are M words in the dictionary and N unique combinations (number of hash_nums), the memory cost of word_dic will be O(M + N) since there are N keys and M values. alpha_dic only costs 42 spaces which is negligible. 

