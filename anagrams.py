import fileinput
import sys


# use prime numbers to represent each letter. The most frequently used letters are associated with smaller primes.
alpha_dic = {'e':2, 't':3, 'a':5, 'o':7, 'i':11, 'n':13, 's':17, 'h':19, 'r':23, 'd':29, 'l':31, 'c':37, 
'u':41, 'm':43, 'w':47, 'f':53, 'g':59, 'y':61, 'p':67, 'b':71, 'v':73, 'k':79, 'j':83, 'x':89, 'q':97, 'z':101, '\'': 103}

# dictionary to store all the words. Keys are the hash_num of the word and values are a list of word with that hash_num
word_dic = {}

#read and storing the dictionary
filename = sys.argv[-1]
with open(filename) as f:
	for line in f:
		word = line.strip().lower()
		hash_num = 1
		# convert each word into a number, anagrams will result in the same number
		for letter in word: 
			hash_num *= alpha_dic[letter]
		if hash_num not in word_dic:
			word_dic[hash_num] = []
		word_dic[hash_num].append(word)



def find_anagrams():
	while True:
		try:
			w = input("")
		except SyntaxError: # quit if the user input an empty string
			sys.exit(0)
		w = w.strip()
		hash_w = 1
		for letter in w:
			hash_w *= alpha_dic[letter]
		if hash_w in word_dic:
			print (" ".join(i for i in sorted(word_dic[hash_w])))
		else:
			print("-")

if __name__ == "__main__":
	find_anagrams()